# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.

class TowersOfHanoi
    attr_reader :towers
    
    def initialize(towers = [[3,2,1], [], []])
        @towers = towers
        @counter = 0
        self.play
    end
    
    def play
        if self.won?(@towers) == true
            p "Congratulations! You've won! You've completed it in #{@counter} moves"
            return
        end
        tower1, tower2 = self.render
        if self.valid_move?(tower1, tower2)
            @counter += 1
            self.move(tower1, tower2)
        else
            puts "That is not a valid move, please try again."
            self.play
        end
    end
    
    def render
        p @towers
        puts "Which tower (1, 2, or 3) would you like to move a disk from?"
        tower1 = gets.chomp.to_i
        puts "Which tower (1, 2, or 3) would you like to move the disk to?"
        tower2 = gets.chomp.to_i
        tower1 -= 1
        tower2 -= 1
        return tower1, tower2
    end
    
    def won?(towers)
        won = false
        if @towers[2].length == 3 && @towers[2] == @towers[2].sort.reverse
            won = true
        elsif @towers[1].length == 3 && @towers[1] == @towers[1].sort.revers
            won = true
        end
        won
    end
    
    def valid_move?(tower1, tower2)
        valid = false
        disc = @towers[tower1].last.to_i
        top_disc = @towers[tower2].last.to_i
        if tower1 == tower2
            valid = false
        elsif @towers[tower1].empty?
            valid = false
        elsif top_disc == 0
            valid = true
        elsif disc < top_disc
            valid = true
        else
            valid = false
        end
        valid
    end
    
    def move(tower1, tower2)
        disc = @towers[tower1].pop
        @towers[tower2].push(disc)
        self.play
    end
end
